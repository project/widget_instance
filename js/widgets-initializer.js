/**
 * @file
 * Drupal behavior to initialize widgets injected on a page.
 */

(function (Drupal) {
  var processedAttrName = 'data-widget-instance-processed';
  function renderWidget(widget) {
    var renderFn = window[widget.renderFunctionName];
    renderFn(
      widget.instanceId,
      widget.language,
      window.location.origin,
      function (domElement) {
        domElement.setAttribute(processedAttrName, true);
        // Emit a renderFinish event.
        domElement.dispatchEvent(new Event('renderFinish'));
      }
    );
  }

  /**
   * This will find all the widgets in a page and it will trigger the render
   * process.
   *
   * @type {{{attach: function}}}
   */
  Drupal.behaviors.widgetsInitializer = {
    attach: function (context, settings) {
      var widgetDefinitions = settings.widget_instance.widgets;
      if (typeof widgetDefinitions !== 'object') {
        return;
      }
      Object.keys(widgetDefinitions).forEach(function (id) {
        var widgetDefinition = widgetDefinitions[id];
        var domElement = document.getElementById(widgetDefinition.instanceId);
        if (typeof window[widgetDefinition.renderFunctionName] !== 'function') {
          return;
        }
        if (domElement && domElement.getAttribute(processedAttrName)) {
          // The widget was rendered already. To force a re-render set
          // 'data-widget-instance-processed' to false.
          return;
        }
        renderWidget(widgetDefinition);
      });
    }
  };
})(Drupal);
