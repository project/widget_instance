<?php

namespace Drupal\Tests\widget_instance\FunctionalJavascript;

use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\JSWebAssert;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\widget_instance\Entity\WidgetInstance;
use Drupal\widget_type\Entity\WidgetType;

/**
 * FunctionalJavascript tests for WidgetInstanceViewBuilder.
 *
 * @group widget_instance
 * @coversDefaultClass \Drupal\widget_instance\WidgetInstanceViewBuilder
 */
class WidgetInstanceTranslationTest extends WebDriverTestBase {

  /**
   * The theme to install as the default for testing.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'widget_instance',
    'widget_type',
    'content_translation',
    'language',
    'views',
  ];

  /**
   * The widget instance.
   *
   * @var \Drupal\widget_instance\WidgetInstanceInterface
   */
  private $widgetInstance;

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  protected function setUp(): void {
    parent::setUp();
    $this->enableTranslation();
    $this->setupLanguages();
    $extension_path_resolver = \Drupal::service('extension.path.resolver');
    assert($extension_path_resolver instanceof ExtensionPathResolver);
    $module_dir = $extension_path_resolver->getPath('module', 'widget_instance');
    $uri = 'base:/' . $module_dir . '/tests/widgets/widget-boilerplate/v1';
    $widget_directory = Url::fromUri($uri, ['absolute' => TRUE])->toString();
    $widget_type = WidgetType::create([
      'name' => 'Boilerplate',
      'remote_widget_id' => 'widget-boilerplate',
      'remote_widget_version' => 'v1.10.2',
      'remote_widget_directory' => $widget_directory,
      'remote_widget_settings' => ['properties' => ['button-text' => NULL]],
      'remote_widget_files' => [
        $widget_directory . '/js/main.js',
        $widget_directory . '/media/logo.png',
      ],
    ]);
    $widget_type->save();
    $this->widgetInstance = WidgetInstance::create([
      'name' => 'foo',
      'type' => $widget_type,
      'display_options' => ['button-text' => 'Lorem Ipsum'],
    ]);
    $this->widgetInstance->save();
  }

  /**
   * @covers ::view
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function testView() {
    $translation = $this->widgetInstance->toArray();
    $translation['display_options'][0] = ['button-text' => 'Lorem Ipsum (es)'];
    $translation['langcode'][0]['value'] = 'es';
    $this->widgetInstance->addTranslation('es', $translation);
    $this->widgetInstance->save();
    $account = $this->drupalCreateUser([
      'access content',
      'view widget instance',
    ]);
    $this->drupalLogin($account);

    $url = $this->widgetInstance->toUrl('canonical', ['language' => ConfigurableLanguage::load('es')]);
    $web_assert = $this->assertSession();
    assert($web_assert instanceof JSWebAssert);
    $this->drupalGet($url);
    // Ensure that a widget is loaded.
    $web_assert->waitForElementRemoved('css', 'div.spinner > div.double-bounce1');
    // The boilerplate widget adds the text Welcome! asynchronously to the page.
    $web_assert->buttonExists('Lorem Ipsum (es)');
  }

  /**
   * Adds additional languages.
   */
  private function setupLanguages() {
    ConfigurableLanguage::createFromLangcode('es')->save();
    $this->rebuildContainer();
  }

  /**
   * Enables translations where it needed.
   */
  private function enableTranslation() {
    // Enable translation for the current entity type and ensure the change is
    // picked up.
    \Drupal::service('content_translation.manager')->setEnabled('widget_instance', 'widget_instance', TRUE);
    drupal_static_reset();
    \Drupal::entityTypeManager()->clearCachedDefinitions();
    \Drupal::service('router.builder')->rebuild();
  }

}
