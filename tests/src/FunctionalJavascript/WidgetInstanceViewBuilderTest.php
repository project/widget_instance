<?php

namespace Drupal\Tests\widget_instance\FunctionalJavascript;

use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\JSWebAssert;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\widget_instance\Entity\WidgetInstance;
use Drupal\widget_type\Entity\WidgetType;

/**
 * FunctionalJavascript tests for WidgetInstanceViewBuilder.
 *
 * @group widget_instance
 * @coversDefaultClass \Drupal\widget_instance\WidgetInstanceViewBuilder
 */
class WidgetInstanceViewBuilderTest extends WebDriverTestBase {

  /**
   * The theme to install as the default for testing.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['widget_instance', 'widget_type', 'views'];

  /**
   * @covers ::view
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function testView() {
    $extension_path_resolver = \Drupal::service('extension.path.resolver');
    assert($extension_path_resolver instanceof ExtensionPathResolver);
    $module_dir = $extension_path_resolver->getPath('module', 'widget_instance');
    $uri = 'base:/' . $module_dir . '/tests/widgets/widget-boilerplate/v1';
    $widget_directory = Url::fromUri($uri, ['absolute' => TRUE])->toString();
    $widget_type = WidgetType::create([
      'name' => 'Boilerplate',
      'remote_widget_id' => 'widget-boilerplate',
      'remote_widget_version' => 'v1.10.2',
      'remote_widget_directory' => $widget_directory,
      'remote_widget_settings' => ['properties' => ['button-text' => NULL]],
      'remote_widget_files' => [
        $widget_directory . '/js/main.js',
        $widget_directory . '/media/logo.png',
      ],
    ]);
    $widget_type->save();
    $entity = WidgetInstance::create([
      'name' => 'foo',
      'type' => $widget_type,
      'display_options' => ['button-text' => 'Lorem Ipsum'],
    ]);
    $entity->save();
    $account = $this->drupalCreateUser([
      'access content',
      'view widget instance',
    ]);
    $this->drupalLogin($account);

    $web_assert = $this->assertSession();
    assert($web_assert instanceof JSWebAssert);
    $this->drupalGet($entity->toUrl());
    // Ensure that a widget is loaded.
    $web_assert->waitForElementRemoved('css', 'div.spinner > div.double-bounce1');
    // The boilerplate widget adds the text Welcome! asynchronously to the page.
    $web_assert->pageTextContainsOnce('Welcome!');
    $web_assert->buttonExists('Lorem Ipsum');
  }

}
