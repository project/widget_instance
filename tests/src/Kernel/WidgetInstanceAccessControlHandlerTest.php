<?php

namespace Drupal\Tests\widget_instance\Kernel;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\widget_instance\Entity\WidgetInstance;
use Drupal\widget_instance\WidgetInstanceAccessControlHandler;
use Drupal\widget_type\Entity\WidgetType;
use Prophecy\Argument;

/**
 * Kernel tests for WidgetInstanceAccessControlHandler.
 *
 * @group widget_instance
 * @coversDefaultClass \Drupal\widget_instance\WidgetInstanceAccessControlHandler
 */
class WidgetInstanceAccessControlHandlerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'widget_type',
    'widget_instance',
    'field',
    'text',
    'image',
    'user',
    'file',
    'system',
    'views',
  ];

  /**
   * The system under test.
   *
   * @var \Drupal\widget_instance\WidgetInstanceAccessControlHandler
   */
  private $theSut;

  /**
   * The entity.
   *
   * @var \Drupal\widget_instance\WidgetInstanceInterface
   */
  private $entity;

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  protected function setUp(): void {
    parent::setUp();
    \Drupal::setContainer($this->container);
    $this->installEntitySchema('widget_type');
    $this->installEntitySchema('widget_instance');
    $this->installEntitySchema('user');
    $this->installConfig(['field', 'widget_instance', 'user']);
    $this->theSut = new WidgetInstanceAccessControlHandler(
      $this->prophesize(EntityTypeInterface::class)->reveal()
    );
    $widget_type = WidgetType::create([
      'name' => 'The name',
      'remote_widget_id' => 'remote-id',
      'remote_widget_version' => 'v1.2.3',
      'remote_widget_directory' => 'https://the-s3/path',
    ]);
    $widget_type->save();
    $this->entity = WidgetInstance::create([
      'title' => 'foo',
      'type' => $widget_type,
    ]);
    $this->entity->save();
  }

  /**
   * @covers ::checkAccess
   * @dataProvider providerCheckAccess
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function testCheckAccess($operation, $permission, $has_permission, $expected) {
    $account = $this->prophesize(AccountInterface::class);
    $account->id()->willReturn(2);
    // All other permissions are considered false.
    $account->hasPermission(Argument::type('string'))->willReturn(FALSE);
    $account->hasPermission($permission)->willReturn($has_permission);
    $this->assertEquals(
      $expected,
      $this->theSut->access($this->entity, $operation, $account->reveal())
    );
  }

  /**
   * Data provider for check access.
   */
  public function providerCheckAccess() {
    return [
      ['view', 'view widget instance', FALSE, FALSE],
      ['view', 'view widget instance', TRUE, TRUE],
      ['update', 'edit widget instance', FALSE, FALSE],
      ['update', 'edit widget instance', TRUE, TRUE],
      ['delete', 'delete widget instance', FALSE, FALSE],
      ['delete', 'delete widget instance', TRUE, TRUE],
      ['update', 'administer widget_instance entities', FALSE, FALSE],
      ['update', 'administer widget_instance entities', TRUE, TRUE],
      ['delete', 'administer widget_instance entities', FALSE, FALSE],
      ['delete', 'administer widget_instance entities', TRUE, TRUE],
    ];
  }

  /**
   * @covers ::checkCreateAccess
   * @dataProvider providerCheckCreateAccess
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function testCheckCreateAccess($permission, $expected) {
    $account = $this->prophesize(AccountInterface::class);
    $account->id()->willReturn(2);
    // All other permissions are considered false.
    $account->hasPermission(Argument::type('string'))->willReturn(FALSE);
    if ($permission) {
      $account->hasPermission($permission)->willReturn(TRUE);
    }
    $this->assertSame(
      $expected,
      $this->theSut->createAccess(NULL, $account->reveal())
    );
  }

  /**
   * Data provider for checkCreateAccess.
   */
  public function providerCheckCreateAccess() {
    return [
      [NULL, FALSE],
      ['create widget instance', TRUE],
      ['administer widget_instance entities', TRUE],
    ];
  }

}
