<?php

namespace Drupal\Tests\widget_instance\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\widget_instance\Entity\WidgetInstance;
use Drupal\widget_type\Entity\WidgetType;

/**
 * Kernel tests for ComputedHtmlIdItemList.
 *
 * @group widget_instance
 * @coversDefaultClass \Drupal\widget_instance\ComputedHtmlIdItemList
 */
class ComputedHtmlIdItemListTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'widget_type',
    'widget_instance',
    'field',
    'text',
    'image',
    'user',
    'file',
    'system',
    'views',
  ];

  /**
   * The entity.
   *
   * @var \Drupal\widget_instance\WidgetInstanceInterface
   */
  private $entity;

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  protected function setUp(): void {
    parent::setUp();
    \Drupal::setContainer($this->container);
    $this->installEntitySchema('widget_type');
    $this->installEntitySchema('widget_instance');
    $this->installEntitySchema('user');
    $this->installConfig(['field', 'widget_type', 'widget_instance', 'user']);
    $widget_type = WidgetType::create([
      'name' => 'The name',
      'remote_widget_id' => 'remote-id',
      'remote_widget_version' => 'v1.2.3',
      'remote_widget_directory' => 'https://the-s3/path',
    ]);
    $widget_type->save();
    $this->entity = WidgetInstance::create([
      'title' => 'foo',
      'type' => $widget_type,
    ]);
    $this->entity->save();
  }

  /**
   * @covers ::getValue
   */
  public function testComputeValue() {
    $this->assertSame('widget-instance--remote-id--1', $this->entity->getHtmlId());
  }

}
