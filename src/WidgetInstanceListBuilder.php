<?php

namespace Drupal\widget_instance;

use Drupal;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\filter\Render\FilteredMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the widget instance entity type.
 */
class WidgetInstanceListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * Constructs a new WidgetInstanceListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, RedirectDestinationInterface $redirect_destination) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $summary = $this->t('Total widget instances: @total', [
      '@total' => $this->getStorage()
        ->getQuery()
        ->count()
        ->execute(),
    ]);
    return [
      'table' => parent::render(),
      'summary' => [
        '#markup' => $summary,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'id' => $this->t('ID'),
      'status' => $this->t('Status'),
      'type' => $this->t('Widget Type'),
      'title' => $this->t('Title'),
      'language' => $this->t('Language'),
      'changed' => $this->t('Updated'),
      'options' => $this->t('Display Options'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function buildRow(EntityInterface $entity) {
    assert($entity instanceof WidgetInstanceInterface);
    $current_language = Drupal::languageManager()->getCurrentLanguage();
    $current_langcode = $current_language->getId();
    if ($entity->hasTranslation($current_langcode)) {
      $entity = $entity->getTranslation($current_langcode);
    }
    // Assert again given the above conditional may have done a re-assignment.
    assert($entity instanceof WidgetInstanceInterface);
    $changed_time = $entity->getChangedTime();
    $display_options = $entity->getDisplayOptions();
    $options = empty($display_options)
      ? ''
      : FilteredMarkup::create(sprintf('<code><pre>%s</pre></code>', json_encode($display_options, JSON_PRETTY_PRINT)));

    return [
      'id' => $entity->id(),
      'status' => $entity->isPublished() ? $this->t('Published') : $this->t('Not published'),
      'type' => $entity->getWidgetType()->toLink(),
      'title' => $entity->toLink(),
      'language' => $entity->language()->getId(),
      'changed' => $changed_time ? $this->dateFormatter->format($changed_time) : '',
      'options' => $options,
    ] + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $destination = $this->redirectDestination->getAsArray();
    foreach (array_keys($operations) as $key) {
      $operations[$key]['query'] = $destination;
    }
    return $operations;
  }

}
