<?php

namespace Drupal\widget_instance;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\widget_type\WidgetTypeInterface;

/**
 * Provides an interface defining a widget instance entity type.
 */
interface WidgetInstanceInterface extends RevisionLogInterface, ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the widget_instance title.
   *
   * @return string
   *   Title of the widget_instance.
   */
  public function getTitle();

  /**
   * Sets the widget_instance title.
   *
   * @param string $title
   *   The widget_instance title.
   *
   * @return \Drupal\widget_instance\WidgetInstanceInterface
   *   The called widget_instance entity.
   */
  public function setTitle($title);

  /**
   * Gets the widget_instance creation timestamp.
   *
   * @return int
   *   Creation timestamp of the widget_instance.
   */
  public function getCreatedTime();

  /**
   * Sets the widget_instance creation timestamp.
   *
   * @param int $timestamp
   *   The widget_instance creation timestamp.
   *
   * @return \Drupal\widget_instance\WidgetInstanceInterface
   *   The called widget_instance entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get the HTML ID.
   *
   * @return string
   *   The HTML ID.
   */
  public function getHtmlId(): string;

  /**
   * Set the widget_instance type.
   *
   * @param \Drupal\widget_type\WidgetTypeInterface $widget_type
   *   The widget_instance type.
   *
   * @return \Drupal\widget_instance\WidgetInstanceInterface
   *   The widget_instance.
   */
  public function setWidgetType(WidgetTypeInterface $widget_type): self;

  /**
   * Get the widget_instance type.
   *
   * @return \Drupal\widget_type\WidgetTypeInterface|null
   *   The widget_instance type.
   */
  public function getWidgetType(): ?WidgetTypeInterface;

  /**
   * Set the display options.
   *
   * @param array $options
   *   The key values to configure the widget_instance JS app.
   *
   * @return \Drupal\widget_instance\WidgetInstanceInterface
   *   The widget_instance.
   */
  public function setDisplayOptions(array $options): self;

  /**
   * Get the display options.
   *
   * @return array
   *   The key values to configure the widget_instance JS app.
   */
  public function getDisplayOptions(): array;

  /**
   * Generates a title when empty.
   *
   * @param \Drupal\widget_instance\WidgetInstanceInterface $entity
   *   The entity.
   *
   * @return string
   *   The generated title.
   */
  public static function generateTitle(WidgetInstanceInterface $entity): string;

}
