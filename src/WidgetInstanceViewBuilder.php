<?php

namespace Drupal\widget_instance;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Language\LanguageInterface;
use Drupal\widget_type\WidgetTypeInterface;

/**
 * {@inheritdoc}
 */
class WidgetInstanceViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    assert($entity instanceof WidgetInstanceInterface);
    $build = parent::view($entity, $view_mode, $langcode);

    $widget_type = $entity->getWidgetType();
    if (!$widget_type instanceof WidgetTypeInterface) {
      return;
    }
    $settings = $widget_type->getSettingsSchema();
    // The settings schema is an object definition, grab the property names.
    $param_names = array_keys($settings['properties'] ?? []);
    $build['#widget_parameters'] = array_intersect_key(
      $entity->getDisplayOptions(),
      array_flip($param_names)
    );
    $build['#widget_parameters']['localecode'] = $langcode ?: $this->languageManager->getDefaultLanguage()->getId();
    $build['#attached']['library'][] = 'widget_instance/widgets_initializer';
    $instance_id = $entity->getHtmlId();
    $translation_code = $langcode === LanguageInterface::LANGCODE_NOT_SPECIFIED
      ? NULL
      : $langcode;
    NestedArray::setValue(
      $build,
      [
        '#attached',
        'drupalSettings',
        'widget_instance',
        'widgets',
        $instance_id,
      ],
      [
        'instanceId' => $instance_id,
        'renderFunctionName' => $widget_type->getJsRenderFunctionName(),
        'language' => $widget_type->negotiateLanguage($translation_code),
        'widgetJsSrc' => $widget_type->getJsSrc(),
      ]
    );

    return $build;
  }

}
