<?php

namespace Drupal\widget_instance\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Drupal\widget_instance\WidgetInstanceInterface;
use Drupal\widget_type\WidgetTypeInterface;
use JsonException;
use SchemaForms\Drupal\FormGeneratorDrupal;
use SchemaForms\FormGeneratorInterface;
use Shaper\Util\Context;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'display options' widget.
 *
 * @FieldWidget(
 *   id = "display_options",
 *   label = @Translation("Display Options"),
 *   field_types = {
 *     "map",
 *   }
 * )
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class DisplayOptionsWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Generates FAPI arrays from JSON Schema definitions.
   *
   * @var \SchemaForms\FormGeneratorInterface
   */
  private $formGenerator;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager, FormGeneratorInterface $form_generator) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->storage = $entity_type_manager->getStorage('widget_type');
    $this->formGenerator = $form_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get(FormGeneratorDrupal::class)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element += [
      '#type' => 'fieldset',
      '#collapsible' => 'false',
      '#attributes' => ['id' => 'display-options-ajax-wrapper'],
      '#tree' => TRUE,
    ];
    $keys = Element::children($form['type']['widget']);
    foreach ($keys as $key) {
      $form['type']['widget'][$key]['#ajax'] = [
        'callback' => [$this, 'widgetSettingsFormAjax'],
        'wrapper' => 'display-options-ajax-wrapper',
      ];
    }
    $input = $form_state->getUserInput();
    // HEY! If the widget type selector ever supports checkboxes for multi
    // values instead of radios, we might have more than one widget type.
    // However, that might not make sense in this case.
    $widget_type_id = (int) NestedArray::getValue($input, array_merge($element['#field_parents'], ['type', 0, 'target_id']))
      ?: NestedArray::getValue($form, ['type', 'widget', '#default_value', 0])
      ?: 0;
    if (!$widget_type_id) {
      $widget_instance = $items->getEntity();
      assert($widget_instance instanceof WidgetInstanceInterface);
      $widget_type = $widget_instance->getWidgetType();
      if (!$widget_type instanceof WidgetTypeInterface) {
        return $element;
      }
      $widget_type_id = $widget_type->id();
    }
    $widget_instance = $items->getEntity();
    if (!$widget_instance instanceof WidgetInstanceInterface) {
      return $element;
    }
    $this->setWidgetTypeSettingsForm($element, $widget_type_id, $widget_instance, $form, $form_state);
    return $element;
  }

  /**
   * Adds the form for the custom options as defined in the form schema.
   *
   * @param array $display_options
   *   The form element to alter.
   * @param int $widget_type_id
   *   The widget type ID.
   * @param \Drupal\widget_instance\WidgetInstanceInterface $widget_instance
   *   The widget instance.
   */
  private function setWidgetTypeSettingsForm(array &$display_options, int $widget_type_id, WidgetInstanceInterface $widget_instance, array $form, FormStateInterface $form_state): void {
    $widget_type = $this->storage->load($widget_type_id);
    assert($widget_type instanceof WidgetTypeInterface);
    $settings_schema = $widget_type->getSettingsSchema();
    if (empty($settings_schema)) {
      $display_options['empty'] = [
        '#markup' => $this->t('<p><em>This widget type does not have configuration options.</em></p>'),
      ];
      return;
    }
    // Encode & decode, so we transform an associative array to an stdClass
    // recursively.
    try {
      $schema = json_decode(
        json_encode($settings_schema, JSON_THROW_ON_ERROR),
        FALSE,
        512,
        JSON_THROW_ON_ERROR
      );
    }
    catch (JsonException $e) {
      $schema = (object) ['type' => 'object', 'properties' => []];
    }
    // Use the form generator to create the form based on the JSON Schema in the
    // widget registry.
    $context = new Context([
      'ui_hints' => $widget_type->getFormSchema(),
      'form_state' => $form_state,
      'form' => $form,
      'current_input' => $widget_instance->getDisplayOptions(),
    ]);
    $display_options[] =$this->formGenerator->transform($schema, $context);
  }

  /**
   * Ajax callback for the form.
   */
  public function widgetSettingsFormAjax(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = array_slice($trigger['#array_parents'], 0, -5);
    return NestedArray::getValue(
      $form,
      array_merge($parents, ['display_options', 'widget', 0])
    );
  }

}
