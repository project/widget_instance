<?php

namespace Drupal\widget_instance\Entity;

use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\user\UserInterface;
use Drupal\widget_instance\ComputedHtmlIdItemList;
use Drupal\widget_instance\WidgetInstanceInterface;
use Drupal\widget_type\WidgetTypeInterface;

/**
 * Defines the widget instance entity class.
 *
 * @ContentEntityType(
 *   id = "widget_instance",
 *   label = @Translation("Widget Instance"),
 *   label_collection = @Translation("Widget Instances"),
 *   handlers = {
 *     "view_builder" = "Drupal\widget_instance\WidgetInstanceViewBuilder",
 *     "list_builder" = "Drupal\widget_instance\WidgetInstanceListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\widget_instance\WidgetInstanceAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "widget_instance",
 *   data_table = "widget_instance_field_data",
 *   revision_table = "widget_instance_revision",
 *   revision_data_table = "widget_instance_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer widget_instance entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "published" = "status",
 *     "status" = "status",
 *     "langcode" = "langcode",
 *     "label" = "title",
 *     "uid" = "uid",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/interactive-components/widget-instance/add",
 *     "canonical" = "/admin/content/interactive-components/widget-instance/{widget_instance}",
 *     "edit-form" = "/admin/content/interactive-components/widget-instance/{widget_instance}/edit",
 *     "delete-form" = "/admin/content/interactive-components/widget-instance/{widget_instance}/delete",
 *     "collection" = "/admin/content/interactive-components/widget-instance"
 *   },
 *   field_ui_base_route = "entity.widget_instance.collection",
 * )
 */
class WidgetInstance extends EditorialContentEntityBase implements WidgetInstanceInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new widget_instance entity is created, set the uid entity reference
   * to the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWidgetType(): ?WidgetTypeInterface {
    return $this->get('type')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setWidgetType(WidgetTypeInterface $widget_type): WidgetInstanceInterface {
    $this->set('type', $widget_type->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHtmlId(): string {
    return $this->get('html_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayOptions(): array {
    try {
      $first = $this->get('display_options')->first();
      if (!$first) {
        return [];
      }
      return $first->getValue();
    }
    catch (MissingDataException $exception) {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setDisplayOptions(array $options): WidgetInstanceInterface {
    $this->set('display_options', $options);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateTitle(WidgetInstanceInterface $entity): string {
    return sprintf(
      'widget-instance--%s--%s',
      $entity->language()->getId(),
      substr(md5(mt_rand()), 0, 6)
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Content ID'))
      ->setDescription(t('Administrative content ID. This is not displayed as part of the widget embed.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setTranslatable(TRUE)
      ->setDefaultValueCallback('Drupal\widget_instance\Entity\WidgetInstance::generateTitle')
      ->setDisplayOptions('form', ['type' => 'hidden'])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the widget_instance author.'))
      ->setSetting('target_type', 'user')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the widget_instance was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the widget_instance was last edited.'));

    $fields += static::customBaseFieldDefinitions();
    return $fields;
  }

  /**
   * The field definitions for the custom business logic.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition[]
   *   The definitions.
   */
  public static function customBaseFieldDefinitions() {
    $fields = [];

    $fields['html_id'] = BaseFieldDefinition::create('string')
      ->setLabel('HTML Id')
      ->setComputed(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setClass(ComputedHtmlIdItemList::class);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Widget Type'))
      ->setDescription(t('The widget definition as ingested from the widget registry.'))
      ->setSetting('target_type', 'widget_type')
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'library',
        ],
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'widget_type_widget_selector',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['display_options'] = BaseFieldDefinition::create('map')
      ->setRevisionable(TRUE)
      ->setLabel(t('Display Options'))
      ->setDescription(t('Each widget can have different settings defined in the widget registry. Provide the necessary values here.'))
      ->setDefaultValue([])
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'display_options',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

}
