<?php

namespace Drupal\widget_instance;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * A computed field item list.
 */
class ComputedHtmlIdItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Compute the list property from the entity.
   */
  protected function computeValue() {
    $entity = $this->getEntity();
    assert($entity instanceof WidgetInstanceInterface);
    $widget_type = $entity->getWidgetType();
    if (!$widget_type) {
      return '';
    }
    $html_id = sprintf(
      'widget-instance--%s--%s',
      $widget_type->getRemoteId(),
      $entity->id()
    );
    $this->list = [$this->createItem(0, $html_id)];
  }

}
