<?php

namespace Drupal\widget_instance;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the widget instance entity type.
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class WidgetInstanceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view widget instance');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit widget instance', 'administer widget_instance entities'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete widget instance', 'administer widget_instance entities'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create widget instance', 'administer widget_instance entities'], 'OR');
  }

}
