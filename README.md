# Widget Instance
This module provides a new entity type called Widget Instance. This is the main content for the widgets. Each instance represents a pair of display options (controlled editorially) and a [Widget Type](https://www.drupal.org/project/widget_type).

Widgets are embedded into Drupal pages by referencing widget instances in drupal pages.

![Screenshot](https://www.drupal.org/files/widget-instance.png)
